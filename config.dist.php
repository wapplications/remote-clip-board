<?php
return (object)[
  # Host for the websocket connection
  'host' => '',
  
  # Port for the websocket connection
  'port' => 1414,
  
  # Passwords for user roles are hard-coded here
  'rolePasswords'=>[
    'normal'=>'',
    'master'=>'',
  ],

  # php command for running the server; >= php 7.2
  'php'=>'php7.2'
];
