(function(){




  // Wait some milliseconds after last code change befor pushing the update
  let updateDelay = 400;

  // CodeMirror Instance
  let myCodeMirror;

  // Theme for CodeMirror Editor
  let editorTheme = 'blackboard'; // 'base16-dark'; // 'pastel-on-dark'; // 'ayu-dark' // 'erlang-dark';

  // remember master passwort
  let masterPassword = null;

  /**
   * Push Code + Mode
   */
  function pushCode() {
    if (!auth.loggedin) return;
    let value = myCodeMirror.getValue();
    let mode = document.getElementById('codeMode').value;
    if (!mode) mode = null;
    mainConnection.send({
      action: 'pushCode',
      data: {
        fullText: value,
        mode: mode
      }
    }, true).then((resp) => {
      let status = resp.status || '';
      if (status === 'ok') {
        return;
      }
      if (status === 'notAllowed') {
        console.log('login required', resp);
        return;
      }
      console.log('pushCode() got: ', resp);
    }).catch((e)=>{
      logger(e, 'error');
    });
  }





  /**
   * Create connection wrapper and action handler
   */
  const mainConnection = (function(){
    const $btn = document.getElementById('btnConnect');
    return createConnection({
      _other(data) {
        logger('unhandled message');
        console.log('_other: ', data);
      },
      closed(evt) {
        // Button visual feedback
        logger('closed.', 'error');
        $btn.classList.remove('online');
        $btn.classList.add('offline');
        $btn.innerText = 'Verbinden';
        auth.loggedin = false;
      },
      opened(evt) {
        // Button visual feedback; show login; load code
        logger('opened.', 'success');
        $btn.classList.remove('offline');
        $btn.classList.add('online');
        $btn.innerText = 'Trennen';
        auth.performLogin(mainConnection, 'master')
        .then(()=>{
          logger('logged in', 'success')
          return loadCode(mainConnection, myCodeMirror);
        })
        .then((text)=>{
          // todo fehler abfangen
          document.getElementById('codeMode').value = text.mode ? text.mode : '';
        })
        .catch((e)=>{
          mainConnection.disconnect();
          console.log(e);
        });
      },
      /**
       * There is a code snippet from a user
       */
      codeFromUser(data) {
        logger('Message from '+data.name)
        UserMessages.add(data.name, data.code);
      }
    });
  })();
  
  


  /**
   * Modul for user messages
   * @interface
   * UserMessages.clear()
   * UserMessages.add(username, code)
   */
  const UserMessages = (function(container) {
    setTimeout(()=>{
      UserMessages.add('Mia', 'Das ist ein Coder 123');
    },1000);

    return  {
      clear() {
        while(container.firstElementChild) {
          container.firstElementChild.remove();
        }
        // @TODO memory leaks?
      },
      add(name, code) {
        let dom = renderDOM({
          name: 'div.user-message.new',
          ref: 'msg',
          children:[
            {name:'div', ref:'open', text: name,}, {name:'div', ref:'delete', text: '×',},
          ]
        });
        function deleteMsg() {
          dom.msg.remove();
        }
        dom.delete.addEventListener('click', deleteMsg);
        dom.open.addEventListener('click', ()=>{
          let msgDialog = dialog([
            {name: 'h1', text: name},
            {name: 'div', ref:'editor'},
            {name: 'div.dialog-buttonrow', styles: {marginTop:'10px'}, children:[
              {name: 'button.light', ref: 'btnDelete', text: 'Löschen'},
              {name: 'button', ref: 'btnSchliessen', text: 'Schließen'}
            ]}
          ], {
            width:'600px',
            afterDOMInsertion(dlg) {
              dlg.refs.cm = CodeMirror(dlg.refs.editor, {
                lineNumbers: true,
                mode: null, // TODO auswählen per dropdown
                theme: editorTheme,
              });
              dlg.refs.cm.setValue(code);
              dlg.refs.btnDelete.addEventListener('click', ()=>{dlg.close(); deleteMsg()});
              dlg.refs.btnSchliessen.addEventListener('click', ()=>{
                dlg.close();
              });
            } // afterDOMInsertion
          });
          msgDialog.open();
        });
        container.appendChild(dom.msg);
        setTimeout(function() {
          dom.msg.classList.remove('new');
        }, 1000);
      }
    }
  })(document.getElementsByTagName('aside').item(0));


  



  /*************************************************
   *                   Buttons                     *
   *************************************************/



  document.getElementById('codeMode').addEventListener('change', (evt)=>{
    let mode = document.getElementById('codeMode').value;
    if (!mode) mode = null;
    myCodeMirror.setOption('mode', mode);
    pushCode();
  });
  
  
  document.getElementById('btnConnect').addEventListener('click', (evt)=>{
    if (mainConnection.isOpen()) {
      mainConnection.disconnect();
    } else {
      mainConnection.connect();
    }
  });


  document.getElementById('btnClearMsg').addEventListener('click', (evt)=>{
    UserMessages.clear();
  });
  
  
  document.getElementById('btnKillServer').addEventListener('click', (evt)=>{
    logger('stop server...');
    http.json_request('post', '/server_control.php', {action: 'stop'}).then((resp)=>{
      if (resp.status === 'ok') {
        logger('ok.', 'success');
      } else {
        logger(resp.status+'.', 'error');
      }
    });
  });
  
  
  document.getElementById('btnStartServer').addEventListener('click', (evt)=>{
    if (!masterPassword) {
      masterPassword = prompt("Enter Master Password:");
    }
    if (!masterPassword) return;
    logger('start server...');
    http.json_request('post', '/server_control.php', {action: 'start', password: masterPassword}).then((resp)=>{
      console.log('server_control', resp);
      switch (resp.status) {
        case 'started':  // server started
          logger('ok (pid: '+resp.pid+').', 'success');
        break;
        case 'taken':    // server already running
          logger('port taken (pid: '+resp.pid+').', 'warn');
        break;
        case 'running':  // other server processes are running
          $result.innerText = 'running';
          let list = resp.processes;
          let msg = 'Potential Servers running:';
          list.forEach(p => {
            msg += "\n" + p.pid + ' ' + p.commad;
          });
          logger('servers running:', 'warn');
          logger(msg);
        break;
        case 'command_failed':
          logger('command failed.', 'error');
        break;
        case 'failed':
          logger('failed: '+resp.message, 'error');
        break;
      }
    });
  });
  
  
  
  
  
  
  
  
  
  

  // Init CodeMirror Editor
  document.addEventListener("DOMContentLoaded", () => {
    myCodeMirror = CodeMirror(document.getElementById('editor'), {
      lineNumbers: true,
      mode: null,
      theme: editorTheme,
    });
    // delay code updates
    (function() {
      let t = null;
      myCodeMirror.on('change', (a,b) => {
        //console.log(a,b);
        if (t) clearTimeout(t);
        t = setTimeout(function() {
          pushCode();
          t = null;
        }, updateDelay);
      });
    })();

    editorResizer(myCodeMirror);
  
  }); // DOMContentLoaded
  
  
  
  
  
  
  
  
  
  
    







})();

