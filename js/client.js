(function(){






  let editorTheme = 'blackboard'; // 'base16-dark'; // 'pastel-on-dark'; // 'ayu-dark' // 'erlang-dark';
  const $btn = document.getElementById('btnConnect');
  let myCodeMirror;

  const mainConnection = createConnection({
    text(data) {
      myCodeMirror.setValue(data.fullText);
      myCodeMirror.setOption('mode', data.mode);
    },
    _other(data) {
      logger('unhandled message');
      console.log('_other: ', data);
    },
    closed(evt) {
      logger('closed.', 'error');
      $btn.classList.remove('online');
      $btn.classList.add('offline');
      $btn.innerText = 'Verbinden';
      auth.loggedin = false;
    },
    opened(evt) {
      logger('opened.', 'success');
      $btn.classList.remove('offline');
      $btn.classList.add('online');
      $btn.innerText = 'Trennen';
      auth.performLogin(mainConnection, 'normal')
      .then((result)=>{
        logger('logged in', 'success')
        document.getElementById('clientName').innerText = result.name;
        return loadCode(mainConnection, myCodeMirror);
      })
      .then((text)=>{
        // todo fehler abfangen
      })
      .catch((e)=>{
        mainConnection.disconnect();
        console.log(e);
      });
    },
  });



  /*************************************************
   *                   Buttons                     *
   *************************************************/

  document.getElementById('btnConnect').addEventListener('click', (evt)=>{
    if (mainConnection.isOpen()) {
      mainConnection.disconnect();
    } else {
      mainConnection.connect();
    }
  });

  document.getElementById('btnCopy').addEventListener('click', async function(evt) {
    let code = myCodeMirror.getValue();
    await navigator.clipboard.writeText(code);
    logger('code copied to clipboard', 'success');
  });

  document.getElementById('btnSendMyCode').addEventListener('click', function(evt) {
    let sendDialog = dialog([
      {name: 'h1', text: 'An Master senden...'},
      {name: 'div', ref:'editor'},
      {name: 'div.dialog-buttonrow', styles: {marginTop:'10px'}, children:[
        {name: 'button', ref: 'btnSend', text: 'Senden'},
        {name: 'button.light', ref: 'btnCancel', text: 'Abbrechen'}
      ]}
    ], {
      width:'600px',
      afterDOMInsertion(dialog) {
        dialog.refs.cm = CodeMirror(dialog.refs.editor, {
          lineNumbers: true,
          mode: null,
          theme: editorTheme,
        });
        dialog.refs.btnSend.addEventListener('click', ()=>{
          let value = dialog.refs.cm.getValue();
          mainConnection.send({action:'pushBackCode', data: {code: value}}, true).then((resp)=>{
            if (resp.status === 'ok') {
              logger('Snippet gesendet.', 'success');
              dialog.close();
            } else {
              logger('Fehler beim senden - siehe JS-Konsole!', 'error');
              console.log('pushBackCode', resp)
            }
          }).catch((e)=>{
            logger('Fehler beim senden - siehe JS-Konsole!', 'error');
            console.log('pushBackCode error',e)
          });
        });
        dialog.refs.btnCancel.addEventListener('click', ()=>{
          dialog.close();
        });
      }
    });
    sendDialog.open();
  });





  // Init CodeMirror Editor
  document.addEventListener("DOMContentLoaded", () => {
    
    myCodeMirror = CodeMirror.fromTextArea(document.getElementById('output'), {
      lineNumbers: true,
      mode: null,
      readOnly: true,
      theme: editorTheme,
    });

    editorResizer(myCodeMirror);
  
  }); // DOMContentLoaded
  














})();

