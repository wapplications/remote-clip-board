<?php

declare(strict_types=1);

require_once __DIR__ . '/ClientUser.php';

use Bloatless\WebSocket\Connection;


/**
 * Websocket Server Application
 *  - Login
 *  - Disribute Code Updates from Master to normal users.
 *  - Forward Code Snippets from Normal Users to Master.
 */
class MainApplication extends \Bloatless\WebSocket\Application\Application {

    private $middleware = [];


    protected function __construct() {
        # @TODO reimplement to go directly in the action methods

        # create fake middleware for actions that ar restricted to specific roles
        $mustBeMaster = mustBeUserOfRole(ClientUser::ROLE_MASTER);
        $mustBeKnown = mustBeUserOfRole([ClientUser::ROLE_MASTER, ClientUser::ROLE_NORMAL]);
        $this->middleware['pushCode'] = $mustBeMaster;
        $this->middleware['pushBackCode'] = $mustBeKnown;
        $this->middleware['loadCode'] = $mustBeKnown;
        # this is not for an action, but to control to whom we will send updates
        $this->middleware['_receiveCode'] = $mustBeKnown;
    }


    /**
     * @var array $clients
     */
    private $clients = [];

    /**
     * Handles new connections to the application.
     * We will wrap the connection in a ClientUser object.
     *
     * @param Connection $client
     * @return void
     */
    public function onConnect(Connection $client): void {
        $id = $client->getClientId();
        $user = new ClientUser($client);
        $this->clients[$id] = $user;
    }

    /**
     * Handles client disconnects.
     *
     * @param Connection $client
     * @return void
     */
    public function onDisconnect(Connection $client): void {
        $id = $client->getClientId();
        unset($this->clients[$id]);
    }

    /**
     * Handles incomming data/requests.
     * If valid action is given the according method will be called.
     *
     * @param string $data
     * @param Connection $client
     * @return void
     */
    public function onData(string $data, Connection $client): void {
        try {
            # find the corresponding 
            $user = $this->clients[$client->getClientId()] ?? null;
            if (!$user) {
                throw new \RuntimeException("Incomming Data from unknown connection");
            }

            # decode action command
            $decodedData = $this->decodeData($data);

            # if the request has a request id, we should include that id in the response
            # to connect the answer with the request.
            $requestId = $decodedData->requestId ?? null;

            # @TODO reimplement - this should go to the methods to make it more clear
            if (isset($this->middleware[$decodedData->action])) {
                $mw = $this->middleware[$decodedData->action];
                if (!$mw($user)) {
                    if ($requestId) {
                        $response = ['status'=>'notAllowed', 'requestId'=>$requestId, 'action'=>$decodedData->action];
                        $user->getConnection()->send(json_encode($response));
                    } else {
                        throw new \RuntimeException("Action ".$decodedData->action." not allowed for User ".$user->getName().' ('.$user->getRole().')');
                    }
                    return;
                }
            }

            # call the action method
            $actionName = 'action' . ucfirst($decodedData->action);
            $client->getServer()->log($actionName);
            if (method_exists($this, $actionName)) {
                call_user_func([$this, $actionName], $decodedData->data, $user, $requestId);
            }

        } catch (\RuntimeException $e) {
            // @todo Handle/Log error
            $client->getServer()->log($e->getMessage());
        }
    }

    ################################################################
    #                         ACTION METHODS                       #
    ################################################################

    /**
     * Perform Login for a role
     * Name is only stored for fun.
     */
    private function actionLogin($data, ClientUser $user, $requestId): void {
        try {
            $config = include __DIR__.'/../config.php';
            $passwords = $config->rolePasswords;
            $name = $data->name;
            $password = $data->password;
            $strRole = $data->role;
            $response = ['status'=>'failed'];
            if ($requestId) {
                $response['requestId'] = $requestId;
            }
            if (empty($name) || empty($password) || empty($strRole)) throw new \Exception("bad request");
            $role = ClientUser::role_from_string($strRole);
            if ($role === null) throw new \Exception("unknown role: ".strRole);
            $passwordForRole = $passwords[$strRole] ?? null;
            if ($passwordForRole === null)  throw new \Exception("no password for role");
            if ($passwordForRole !== $password) throw new \Exception("auth failed");
            $user->setName($name);
            $user->setRole($role);
            $response['status'] = 'ok';
            $user->getConnection()->getServer()->log('login OK for '.$name);
        } catch (\Exception $e) {
            $user->getConnection()->getServer()->log('login faled from '.$name.' - '.$e->getMessage());
            $response['msg'] = $e->getMessage();
        }
        $user->getConnection()->send(json_encode($response));
    }

    /**
     * Action pushBackCode
     * A normal user sends a snippet to the master
     */
    private function actionPushBackCode($data, ClientUser $user, $requestId): void {
        $update = $this->encodeData('codeFromUser', ['code' => $data->code, 'name' => $user->getName()]);
        $fromId = $user->getConnection()->getClientId();
        foreach ($this->clients as $u) {
            if ($u->getConnection()->getClientId() === $fromId) continue;
            if ($u->getRole() !== ClientUser::ROLE_MASTER) continue;
            $u->getConnection()->send($update);
        }
        $response = ['status'=>'ok'];
        if ($requestId) {
            $response['requestId'] = $requestId;
        }
        $user->getConnection()->send(json_encode($response));
    }


    /**
     * Action pushCode
     * The master updates the Code.
     * the update will be distributed to all normal users.
    */
    private function actionPushCode($data, ClientUser $user, $requestId): void {
        $this->distributeCode($data, $user);
        $response = ['status'=>'ok'];
        if ($requestId) {
            $response['requestId'] = $requestId;
        }
        $user->getConnection()->send(json_encode($response));
        $this->storeCode($data);
    }

    /**
     * action loadCode
     * A User loads the code at startup.
     */
    private function actionLoadCode($data, ClientUser $user, $requestId): void {
        $text = $this->loadCode();
        $response = ['status'=>'ok', 'text'=>$text];
        if ($requestId) {
            $response['requestId'] = $requestId;
        }
        $user->getConnection()->send(json_encode($response));
    }

    ################################################################

    /**
     * Distributing code updates
     */
    private function distributeCode($data, ?ClientUser $exclude) {
        $mw = $this->middleware['_receiveCode']; // @TODO reimplement middleware
        $fromId = null;
        if ($exclude) {
            $fromId = $exclude->getConnection()->getClientId();
        }
        $update = $this->encodeData('text', $data);
        foreach ($this->clients as $sendto) {
            if ($sendto->getConnection()->getClientId() === $fromId) continue;
            if (!$mw($sendto)) continue;
            $sendto->getConnection()->send($update);
        }
    }


    /**
     * Load Code from store file - supprise!
     */
    private function loadCode() {
        $default = (object)['mode'=>'htmlmixed', 'fullText'=>''];
        $fileName = realpath('./store/').'/1414.json';
        if (!file_exists($fileName)) {
            return $default;
        }
        $result = json_decode(file_get_contents($fileName));
        if (!$result) {
            return $default;
        }
        return $result;
    }

    /**
     * Store Code to store file - this code is full of supprises!
     */
    private function storeCode($data) {
        $fileName = realpath('./store/').'/1414.json';
        file_put_contents($fileName, json_encode($data, JSON_PRETTY_PRINT));
    }

}


/**
 * Fake-Middleware Helper
 * @TODO reimplement middleware
 */
function mustBeUserOfRole($roles) {
    if (!is_array($roles)) {
        $roles = [$roles];
    }
    return function(ClientUser $user) use ($roles) {
        return in_array($user->getRole(), $roles);
    };
}
