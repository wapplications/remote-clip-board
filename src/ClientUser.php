<?php

use Bloatless\WebSocket\Connection;

/**
 * Wrapper for websocket connections for storing user name and roles
 */
class ClientUser {
  const ROLE_NONE = 0;
  
  # normal user that receives code updates
  const ROLE_NORMAL = 1;

  # master controls server and distributes code
  const ROLE_MASTER = 2;

  private $name = 'unbekannt';
  private $role = self::ROLE_NONE;

  # Socket connection for this user
  private $connection = null;

  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  static function role_from_string(string $role): ?int {
    static $mapping = [
      'master'=>self::ROLE_MASTER,
      'normal'=>self::ROLE_NORMAL,
    ];
    return $mapping[$role] ?? null;
  }

  function setRole(int $role) {
    $this->role = $role;
  }

  function setName(string $name) {
    $this->name = $name;
  }

  function getRole(): int {
    return $this->role;
  }

  function getName(): string {
    return $this->name;
  }

  function getConnection(): Connection {
    return $this->connection;
  }


}

