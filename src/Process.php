<?php
/* An easy way to keep in track of external processes.
* Ever wanted to execute a process in php, but you still wanted to have somewhat controll of the process ? Well.. This is a way of doing it.
* @compability: Linux only. (Windows does not work).
* @author: Peec
* @author Stefan Beyer
*/
class Process{
    private $pid;
    private $command;

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            $this->runCom();
        }
    }

    /**
     * Run command in background and return pid.
     */
    private function runCom(){
        $command = 'nohup '.$this->command.' > /dev/null 2>&1 & echo $!';
        $handle = popen($command, 'r');
        $op = fread($handle, 2096);
        pclose($handle);
        $this->pid = intval($op);
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    /**
     * Check if the process is running
     */
    public function status(){
        $command = 'ps -p '.$this->pid;
        exec($command,$op);
        if (!isset($op[1]))return false;
        else return true;
    }

    public function start(){
        if ($this->command != '') $this->runCom();
        else return true;
    }

    public function stop(){
        $command = 'kill '.$this->pid;
        exec($command);
        if ($this->status() == false)return true;
        else return false;
    }


    /**
     * Find a process that listens to a port
     * @return int | null
     */
    static public function findForPort($port, $protocol = 'tcp') {
        $search = $port.'/'.$protocol;
        $command = 'fuser ' .  escapeshellarg($search);
        exec($command, $op);
        $op = $op[0] ?? null;
        if (!$op) {
            return null;
        }
        if (!preg_match('`\\s*(\\d+)$`', $op, $m)) {
            return null;
        }
        return intval($m[1]);
    }


    /**
     * find a process whose cmd matches a regular expression
     * @return array of pids
     */
    static public function find($pattern) {
        $command = 'ps -ao pid,args --no-headers';
        exec($command,$op);
        $patt = '`^([\\s\\d]+) (.*)$`';

        $found = [];
        foreach ($op as $o) {
            if (preg_match($patt, $o, $m)) {
                $pid = trim($m[1]);
                $command = trim($m[2]);
                if (preg_match($pattern, $command, $_m)) {
                    $found[] = ['pid' => $pid,'command' => $command, 'matches' => $_m];
                }
            }
        }
        return $found;
    }
      




}
