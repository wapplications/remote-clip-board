<?php
require_once './src/Process.php';
$config = include './config.php';


/**
 * Read raw request body as JSON.
 * Fallback: read JSON from query string.
 * @TODO outsource!
 */
$json = file_get_contents('php://input');
$request = json_decode($json);
if (!$json || !$request) {
  $request = (object)$_GET;
}

/**
 * Extract action and construct action callback name.
 */
$action = $request->action ?? null;
if (!$action) {
  json([
    'status' => 'no_action',
  ]);
}
$method = 'action' . ucfirst($action);
if (!function_exists($method)) {
  json([
    'status' => 'failed',
    'message'=>'action not implemented: '.$action
  ]);
}

/**
 * Execute action callback
 */
$method($request);



#######################################################
#                  ACTION CALLBACKS                   #
#######################################################


/**
 * Stop the server process.
 * Tries to find the process, litening on the cogifured port
 * and kills it.
 */
function actionStop($request) {
  $port = config('port');
  if (!$port) {
    json([
      'status' => 'no_port',
      'message' => 'no port'
    ]);
  }

  # Find process for port
  $pid = Process::findForPort($port);
  if (!$pid) {
    json(['status' => 'no_pid', 'port' => $port]);
  }

  # kill that process
  $process = new Process();
  $process->setPid($pid);
  $process->stop();

  # whait and test, if the process is not running any more
  sleep(1);
  if ($process->status()) { # still running...
    json([
      'status' => 'command_failed',
    ]);  
  }
  json([
    'status' => 'ok',
  ]);  
}




/**
 * Publishes the current host:port configuration for client connections.
 */
function actionGetHost($request) {
  json([
    'host' => config('host'),
    'port' => config('port'),
  ]);
}




/**
 * Start the server process.
 * 
 */
function actionStart($request) {
  $host = config('host');
  $port = config('port');

  // TODO use $request->password

  # port taken? server is probably already running
  $pid = Process::findForPort($port);
  if ($pid) {
    json(['status' => 'taken', 'port' => $port, 'pid' => $pid]);
  }
  
  # create command for the server
  $php = config('php');
  $commandBase = $php.' ./cli/server.php';
  $pattern = '`^'.preg_quote($commandBase, '`').'`';
  
  # search for processes with the same command
  $find = Process::find($pattern);
  if (count($find)>0) {
    json([
      'status' => 'running',
      'processes' => $find,
    ]);
  }

  # build the command and run it...
  $command = $commandBase . ' ' . escapeshellarg($host) . ' ' . escapeshellarg($port);
  try {
    # runs the command
    $process = new Process($command);
    # wait a moment and check if it is running
    sleep(1);
    if ($process->status()) {
      json([
        'status' => 'started',
        'pid' => $process->getPid(),
        'port'=>$port,
      ]);  
    }
    json([
      'status' => 'command_failed',
    ]);  
  } catch (\Exception $e) {
    json([
      'status' => 'failed',
      'message' => $e->getMessage()
    ]);  
  }
}






/**
 * Create JSON response and finish this script 
 */
function json($data) {
  header('Content-Type:application/json');
  $data = json_encode($data);
  header('Content-Length: '.strlen($data));
  echo $data;
  ob_flush();
  flush();
  die();
}


/**
 * Helper function for accessing the config 
 */
function config($key) {
  global $config;
  return $config->{$key} ?? null;
}
