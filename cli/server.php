<?php
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');
ini_set('html_errors', 'on');
ini_set('log_errors', 'on');
error_reporting(-1);

# redirect all output to log file
ob_start(function($b,$ph) {
  @file_put_contents('./server.log', $b, FILE_APPEND);
});


# we don't want browsers here, sorry bro!
if (php_sapi_name() !== 'cli') {
  echo 'cli only';
  ob_flush();
  die();
}


require __DIR__ . '/../src/php-websocket/Connection.php';
require __DIR__ . '/../src/php-websocket/Socket.php';
require __DIR__ . '/../src/php-websocket/Server.php';
require __DIR__ . '/../src/php-websocket/Application/ApplicationInterface.php';
require __DIR__ . '/../src/php-websocket/Application/Application.php';
require __DIR__ . '/../src/MainApplication.php';


echo date("d.m.Y"), PHP_EOL;
ob_flush();

# Host and port from command args
$ip = isset($argv[1]) ? $argv[1] : null;
if (!$ip) {
  ob_flush();
  die('no ip');
}

$port = isset($argv[2]) ? intval($argv[2]) : null;
if (!$port) {
  ob_flush();
  die('no port');
}


echo 'Will be listening on: ', $ip, ':', $port, PHP_EOL;
ob_flush();

# log daten vom server auf normale ausgabe umleiten
\Bloatless\WebSocket\Server::$logger = function($message, $type='warn') {
  echo date('Y-m-d H:i:s') . ' [' . ($type ? $type : 'error') . '] ' . $message . PHP_EOL;
  \ob_flush();
};


# set up and run server
$server = new \Bloatless\WebSocket\Server($ip, $port);
$server->setMaxClients(50);
$server->setCheckOrigin(false);
$server->setAllowedOrigin('foo.lh');
$server->setMaxConnectionsPerIp(50);
$server->setMaxRequestsPerMinute(1000);
$server->registerApplication('main', \MainApplication::getInstance());
$server->run();

echo 'Unexpected End: ', date("d.m.Y"), PHP_EOL;
ob_flush();
